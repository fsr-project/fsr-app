export interface GroupContact {
    idContact: number;
    firstname: string;
    lastname: string;
}