// ContactOverview.ts
import {CommonContact} from "@/domain/contact/entity/common/CommonContact";

export interface ContactOverview extends CommonContact{
    idContact: number;
}
