import {CommonAddress} from "@/domain/contact/entity/common/CommonAddress";

export interface Address extends CommonAddress{
    idAddress: number;
}