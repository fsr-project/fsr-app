import {CommonGroup} from "@/domain/CommonGroup";

export interface ContactGroup extends CommonGroup{
    idContactGroup: number;
}