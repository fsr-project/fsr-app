export interface CommonPhoneNumber {
    phoneKind: string;
    phoneNumber: string;
}