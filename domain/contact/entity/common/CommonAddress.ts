export interface CommonAddress {
    country: string;
    zip: string;
    city: string;
    street: string;
}