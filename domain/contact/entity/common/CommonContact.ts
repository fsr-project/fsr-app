export interface CommonContact {
    firstName: string;
    lastName: string;
    email: string;
}