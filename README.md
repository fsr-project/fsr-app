# FSR-APP

## It's time to develop 👨‍💻
FSR is a web application designed to simplify the way you manage your contacts and stay organized. Whether it's adding, modifying, or deleting contacts, or efficiently organizing them into customized groups, FSR offers a seamless and user-friendly experience. Built using the power of Spring and Hibernate.

## Getting started  🏎

To make it easy for you to get started with this project, here's a list of recommended next steps.

- [INSTALL WSL DEBIAN](https://www.linuxfordevices.com/tutorials/linux/install-debian-on-windows-wsl)
- [INSTALL DOCKER](https://docs.docker.com/engine/install/debian/#install-from-a-package)
- [INSTALL MAKE](https://installati.one/debian/11/make/)
- [INSTALL PHPSTORM](https://www.jetbrains.com/shop/eform/students)

## Clone project  🧬

    Create this tree project and clone the project : 

```
     cd && mkdir fsr-project && cd fsr-project && git clone https://gitlab.com/fsr-project/fsr-app.git
```

## Define git global information    👀

    add your personal informations (git information): 

```    
    git config --global user.email "gitlab-email" \
         &&  git config --global user.name "gitlab-name"

```

## Start project    🏁
    Always run these commands to start a project : 

```
    sudo service docker start
```

```
    make install
    make start
```
